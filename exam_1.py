import re
import requests
import asyncio

class WeatherApi:
    apitoken = '0fb610dab7456bc44dbdde2ddba9be71&units=metric%27'
    def get_url(self, city):
        return fr'http://api.openweathermap.org/data/2.5/find?q={city},RU&type=like&APPID={self.apitoken}'

api = WeatherApi()

class City:
    def __init__(self, name, population):
        self.name = name
        self.population = population

    async def get_weather(self):
        url = api.get_url(self.name)
        r = requests.get(url)
        if r.status_code == 200:
            temp = r.json()['list'][0]['main']['temp'] - 273.15
            self.temp = temp
        else:
            raise ConnectionError

class CityHolder:
    def __init__(self, cities):
        self.cities = cities

    async def _get_weather(self):
        tasks = []
        for city in self.cities:
            tasks.append(asyncio.create_task(city.get_weather()))

        for task in tasks:
            await task

    def get_weather(self):
        asyncio.run(self._get_weather())
        return self
    
    def __str__(self):
        s = ''
        for city in self.cities:
            s += f'{city.name}\t{city.temp:0.3f} Co\n'
        return s


if __name__ == '__main__':
    with open(r'cities.txt', 'r', encoding = 'utf-8') as f:
        lines = f.readlines()

    cities = []
    for line in lines:
        if not re.match('\d[)].*', line):
            continue
        line = line.replace(',', '.')
        spl_line = re.split('\d', line[2:])
        cityname = spl_line[0].replace('-','').strip()
        population = re.search('\d{1,}([.]\d{1,}|(\d*))', line[2:])
        cities.append(City(cityname, float(population.group())))

    citydata = CityHolder(cities).get_weather()
    print(citydata)
    print('test jenkins')

